# Exercise 2

This is the second exercise of a three part test. It consists of a GUI that shows a progress bar together with two buttons (Start & Stop). If *Start* is pressed a secondary process is launched and the progress bar is updated as it goes. This secondary process runs a python program that waits for 5 seconds.

### Requirements

To properly execute the program it is required to have installed:

* Python 3.6 or above.
* PyQt5


### Usage

Note that our working direcotry shold be *Exercise_2/*, or specifiy in the command argument the path */Exercise_2/main.py*. A basic usage command would be:

**Running the program in Exercise_2/ as working directory:**
```
python3 main.py
```

**Initial GUI**

Here we can see the GUI that we will see once we execute the program.

![Initial GUI](Images/GUI_Basic.png)

**Start button pressed**

If we were to press *Start*, the program would engage in a process. That is visible in the progress bar that will update along the 5 seconds that is expected to last.

![Start burron pressed](Images/GUI_Started.png)

**Process finished**

Once the timer finishes, the GUI stops at 100%, leaving the program finished in that state.

![Process finished](Images/GUI_Completed.png)
