import sys
from pathlib import Path
from PyQt5.QtWidgets import (QWidget, QProgressBar, QPushButton, QApplication)
from PyQt5.QtCore import QBasicTimer, QProcess

class ProgressBarGUIClass(QWidget):
    def __init__(self, parent = None):
        super().__init__(parent)

        # Initialize QProcess requirements.
        self.process = QProcess()
        self.program = "python"
        self.path = [str(Path().absolute()) + "/process.py"]

        # Initialize visual GUI elements.
        self.progressBar = QProgressBar(self)
        self.progressBar.setGeometry(30, 40, 200, 25)
        self.max = 100

        self.btnStart = QPushButton('Start', self)
        self.btnStart.move(30, 80)
        self.btnStart.clicked.connect(self.launch_process)

        self.btnStop = QPushButton('Stop', self)
        self.btnStop.move(150, 80)
        self.btnStop.clicked.connect(self.kill_process)

        # Initialize QTimer values.
        self.timer = QBasicTimer()
        self.step = 0
        self.time = 50

    def reset_progress(self):
        """
        Resets the progress bar to inital values.
        """
        self.step = 0
        self.progressBar.setValue(self.step)

    def timerEvent(self, event):
        """
        Updates progress bar based on a counter.
        """
        if self.step >= self.max:
            self.timer.stop()
            return
        self.step += 1
        self.progressBar.setValue(self.step)

    def launch_process(self):
        """
        Launches a process that will run a python program which waits for 5 seconds. I will also start the basic timer for the GUI progress bar.
        """
        if self.timer.isActive():
            return
        self.reset_progress()
        self.process.start(self.program, self.path)
        self.timer.start(self.time, self)


    def kill_process(self):
        """
        Aborts the process in case its launched. Aferwards, it resets all the values needed.
        """
        if not self.timer.isActive():
            return
        self.process.kill()
        self.timer.stop()
        self.reset_progress()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = ProgressBarGUIClass()
    window.show()
    app.exec_()
